const XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;

class Dutate {
    constructor(token){
        this.token = token;
    }
    track(event,extra){
        const xhr = new XMLHttpRequest();
        const url = `https://api.dutate.com/track/event/${event}/`;
        xhr.open("POST", url, true);
        xhr.setRequestHeader('Content-Type', 'application/json; utf-8');
        xhr.setRequestHeader('Authorization', `Bearer ${this.token}`);
        xhr.send(JSON.stringify(extra));
    }
}

module.exports = Dutate;