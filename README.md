# dutate-js

This library provides easy way to track your events to Dutate. 
It is simple to use and fully asyncronous.

## Installation
`npm install dutate-js`

## Quick Start
```
const Dutate = require('dutate-js');

const dutate = new Dutate("<your-token>");
dutate.track("event name");
```
